import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDescripcionInstrumentoComponent } from './form-descripcion-instrumento.component';

describe('FormDescripcionInstrumentoComponent', () => {
  let component: FormDescripcionInstrumentoComponent;
  let fixture: ComponentFixture<FormDescripcionInstrumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDescripcionInstrumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDescripcionInstrumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
