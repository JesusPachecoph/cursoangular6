import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DescripcionInstrumento } from '../models/descripcion-instrumento.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-descripcion-instrumento',
  templateUrl: './form-descripcion-instrumento.component.html',
  styleUrls: ['./form-descripcion-instrumento.component.css']
})
export class FormDescripcionInstrumentoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DescripcionInstrumento>;
  fg: FormGroup;
  longitudMin = 4;
  searchResults: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        //this.nombreValidator,
        this.nombreValidatorParams(this.longitudMin)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log("Cambió el formulario:", form);
    })
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length >2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(()=> ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse =>{
        this.searchResults = ajaxResponse.response;
      });
  }
  guardar(nombre: string, url: string): boolean {
    const d = new DescripcionInstrumento(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < this.longitudMin) {
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParams(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true };
      }
      return null;
    }
  }

}
