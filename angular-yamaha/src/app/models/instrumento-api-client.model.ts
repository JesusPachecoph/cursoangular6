import { DescripcionInstrumento } from './descripcion-instrumento.model';
import { Subject, BehaviorSubject } from 'rxjs';

export class InstrumentoApiClient {
	instrumentos:DescripcionInstrumento[];
	current: Subject<DescripcionInstrumento> = new BehaviorSubject<DescripcionInstrumento>(null);
	constructor() {
       this.instrumentos = [];
	}
	add(d:DescripcionInstrumento){
	  this.instrumentos.push(d);
	}
	getAll(){
	  return this.instrumentos;
	}
	
	getById(id: String): DescripcionInstrumento{
		return this.instrumentos.filter(function(d) {return d.id.toString()===id})[0];
	}

	elegir(d: DescripcionInstrumento){
		//this.instrumentos.forEach(x ==> x.setSelected(false));
		this.instrumentos.forEach(function(x){x.setSelected(false)});
		d.setSelected(true);
		this.current.next(d);
	}

	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}
}