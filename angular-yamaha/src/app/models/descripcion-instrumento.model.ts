export class DescripcionInstrumento{
	private selected : boolean;
	contenido : String[];
	id: number;
	constructor(public nombre:string, public imagenUrl:string){
		this.contenido = ["Instructivo","Kit de limpieza"];
	}
	//nombre:string;
	//imagenUrl:string;

	/*constructor(n:string, i:string){
		this.nombre=n;
		this.imagenUrl=i;
	}
*/
	isSelected():boolean{
		return this.selected;
	}

	setSelected(s: boolean){
		this.selected = s;
	}
}