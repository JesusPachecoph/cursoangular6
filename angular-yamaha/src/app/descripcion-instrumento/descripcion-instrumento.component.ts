import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import {DescripcionInstrumento} from './../models/descripcion-instrumento.model';

@Component({
  selector: 'app-descripcion-instrumento',
  templateUrl: './descripcion-instrumento.component.html',
  styleUrls: ['./descripcion-instrumento.component.css']
})
export class DescripcionInstrumentoComponent implements OnInit {
  @Input() descripcion:DescripcionInstrumento;
  @Input('idx') indice : number;
	@HostBinding('attr.class') cssClass = 'col-md-4';
	@Output() presionado: EventEmitter<DescripcionInstrumento>
  constructor() { 
    this.presionado = new EventEmitter();
  }

  ngOnInit(): void {
  }
  
  ir():boolean{
    this.presionado.emit(this.descripcion);
    return false; //para que no recargue la página
  }
  
}
