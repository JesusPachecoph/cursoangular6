import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescripcionInstrumentoComponent } from './descripcion-instrumento.component';

describe('DescripcionInstrumentoComponent', () => {
  let component: DescripcionInstrumentoComponent;
  let fixture: ComponentFixture<DescripcionInstrumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescripcionInstrumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescripcionInstrumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
