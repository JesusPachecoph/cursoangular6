import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes} from "@angular/router";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ListInstrumentosComponent } from './list-instrumentos/list-instrumentos.component';
import { DescripcionInstrumentoComponent } from './descripcion-instrumento/descripcion-instrumento.component';
import { DetalleInstrumentoComponent } from './detalle-instrumento/detalle-instrumento.component';
import { FormDescripcionInstrumentoComponent } from './form-descripcion-instrumento/form-descripcion-instrumento.component';
import { InstrumentoApiClient } from "./models/instrumento-api-client.model";

const misrutas: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ListInstrumentosComponent},
  {path: 'detalle', component: DetalleInstrumentoComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    ListInstrumentosComponent,
    DescripcionInstrumentoComponent,
    DetalleInstrumentoComponent,
    FormDescripcionInstrumentoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(misrutas)
  ],
  providers: [InstrumentoApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
