import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListInstrumentosComponent } from './list-instrumentos.component';

describe('ListInstrumentosComponent', () => {
  let component: ListInstrumentosComponent;
  let fixture: ComponentFixture<ListInstrumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListInstrumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListInstrumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
