import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DescripcionInstrumento } from './../models/descripcion-instrumento.model';
import { InstrumentoApiClient } from "./../models/instrumento-api-client.model";

@Component({
  selector: 'app-list-instrumentos',
  templateUrl: './list-instrumentos.component.html',
  styleUrls: ['./list-instrumentos.component.css']
})
export class ListInstrumentosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DescripcionInstrumento>;
  updates: string[];
  constructor(public instrumentoApiClient: InstrumentoApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.instrumentoApiClient.subscribeOnChange((d: DescripcionInstrumento) => {
      if(d != null){
        this.updates.push("Se ha elegido a " + d.nombre);
        console.log("Se ha elegido a " + d.nombre);
      }
    })
    //this.onItemAdded = new EventEmitter();
  }
  //instrumentos:DescripcionInstrumento[];
  /*
    constructor() { 
       this.instrumentos= []
      }
  */
  ngOnInit(): void { }

  agregado(d: DescripcionInstrumento) {
    this.instrumentoApiClient.add(d);
    this.onItemAdded.emit(d); //no es necesario
  }

  elegido(d: DescripcionInstrumento) {
    this.instrumentoApiClient.elegir(d);
    //this.instrumentos.forEach(function(x){x.setSelected(false)});
    //this.instrumentoApiClient.getAll().forEach(x => x.setSelected(false));
    //d.setSelected(true);
  }
  /*
    guardar(nombre:string, url:string):boolean{
      this.instrumentos.push(new DescripcionInstrumento(nombre,url));
      return false;
    }
  */
}
